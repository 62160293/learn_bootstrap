module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160293/learn_bootstrap/'
    : '/'
}
